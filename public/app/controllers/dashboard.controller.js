'use strict';

var app = angular.module('app');

app.controller('DashboardCtrl', ['$scope',function($scope) {

    $scope.comboChartData = [
        ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
        ['2014/05', 165, 938, 522, 998, 450, 614.6],
        ['2014/06', 135, 1120, 599, 1268, 288, 682],
        ['2014/07', 157, 1167, 587, 807, 397, 623],
        ['2014/08', 139, 1110, 615, 968, 215, 609.4],
        ['2014/09', 136, 691, 629, 1026, 366, 569.6]
    ];

    $scope.salesData = [
        ['Year', 'Sales', 'Expenses'],
        ['2010', 1000, 400],
        ['2011', 1170, 460],
        ['2012', 660, 1120],
        ['2013', 1030, 540]
    ];
}

]);
app.controller('ProgressDemoCtrl', [
    '$scope', function($scope) {
        $scope.max = 200;
        $scope.random = function() {
            var type, value;
            value = Math.floor((Math.random() * 100) + 10);
            type = void 0;
            if (value < 25) {
                type = 'success';
            } else if (value < 50) {
                type = 'info';
            } else if (value < 75) {
                type = 'warning';
            } else {
                type = 'danger';
            }
            $scope.showWarning = type === 'danger' || type === 'warning';
            $scope.dynamic = value;
            $scope.type = type;
        };
        return $scope.random();
    }
]);