'use strict';
/*global _:false */

var app = angular.module('app');

app.controller('NavCtrl', [
    '$scope', 'taskStorage', 'filterFilter', function($scope, taskStorage, filterFilter) {
        var tasks;
        tasks = $scope.tasks = taskStorage.get();
        $scope.taskRemainingCount = filterFilter(tasks, {
            completed: false
        }).length;
        return $scope.$on('taskRemaining:changed', function(event, count) {
            $scope.taskRemainingCount = count;
            return $scope.taskRemainingCount;
        });
    }
]);
app.controller('AppCtrl', ['$scope','$location','$http','Global', function($scope,$location,$http, Global) {
    $scope.user = Global.user;
    $scope.isSpecificPage = function(){
        var path = $location.path();
        return _.contains( ['/404', '/500', '/login', '/register'], path );
    };

}]);
