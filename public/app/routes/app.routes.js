'use strict';

//Setting up route
angular.module('app').config(['$stateProvider','$urlRouterProvider',
    function($stateProvider,$urlRouterProvider) {

        // states for my app
        $stateProvider
            .state('app.dashboard', {
                url: '/dashboard',
                views: {
                    'view@': {
                        templateUrl: 'public/app/views/dashboard.html'
                    }
                }
            })
            .state('app.ui', {
                url: '/ui'
            }).state('app.ui.typography',{
                url: '/typography',
                views: {
                    'view@': {
                        templateUrl: 'public/app/views/ui.typography.html'
                    }
                }
            }).state('app.ui.widgets',{
                url: '/widgets',
                views: {
                    'view@': {
                        templateUrl: 'public/app/views/ui.widgets.html'
                    }
                }
            }).state('app.tasks',{
                url: '/tasks',
                views: {
                    'view@': {
                        templateUrl: 'public/app/views/tasks.html'
                    }
                }
        });
    }
]);