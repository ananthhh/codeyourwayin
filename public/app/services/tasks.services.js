'use strict';
/*global toastr:false */

var app = angular.module('app');

app.factory('taskStorage', function() {
    var DEMO_TASKS, STORAGE_ID;
    STORAGE_ID = 'tasks';
    DEMO_TASKS = '[ {"title": "Finish homework", "completed": true},'+
        '{"title": "Make a call", "completed": true},'+
        '{"title": "Play games with friends", "completed": false},'+
        '{"title": "Shopping", "completed": false}'+
    ']';
    return {
        get: function() {
            return JSON.parse(localStorage.getItem(STORAGE_ID) || DEMO_TASKS);
        },
        put: function(tasks) {
            return localStorage.setItem(STORAGE_ID, JSON.stringify(tasks));
        }
    };
});

app.factory('logger', [
    function() {
        var logIt;
        toastr.options = {
            'closeButton': true,
            'positionClass': 'toast-bottom-right',
            'timeOut': '3000'
        };
        logIt = function(message, type) {
            return toastr[type](message);
        };
        return {
            log: function(message) {
                logIt(message, 'info');
            },
            logWarning: function(message) {
                logIt(message, 'warning');
            },
            logSuccess: function(message) {
                logIt(message, 'success');
            },
            logError: function(message) {
                logIt(message, 'error');
            }
        };
    }
]);