'use strict';
/*global $:false */

var app = angular.module('app');

app.directive('collapseNav', [ function() {
        return {
            restrict: 'A',
            compile: function(ele, attrs) {
                var lists = ele.find('ul').parent('li');
                lists.append('<i class="fa fa-caret-right icon-has-ul"></i>');
                var a = lists.children('a');
                var listsRest = ele.children('li').not(lists);
                var aRest = listsRest.children('a');

                a.on('click', function (event) {
                        var $this = $(this);
                        var parent = $this.parent('li');
                        lists.not(parent).removeClass('open').find('ul').slideUp();
                        parent.toggleClass('open').find('ul').slideToggle();

                        event.preventDefault();
                    }
                );

                aRest.on('click', function (event) {
                    lists.removeClass('open').find('ul').slideUp();
                });
            }
        };
    }]

);

app.directive('slimScroll', [ function() {
    return {
        restrict: 'A',
        link: function(scope, ele, attrs) {
            ele.slimScroll({
                height: '100%'
            });
        }
    };
}
]);

// add background and some style just for specific page
app.directive('customBackground',function(){
        return{
            restrict:'A',
            controller:['$scope','$element','$location',function($scope,$element,$location){
                var addBg,path;
                return path=function(){
                    return $location.path();
                },
                    addBg=function(path){
                        switch($element.removeClass('body-home body-special body-tasks'),path){
                            case'/':return $element.addClass('body-home');
                            case'/404':
                            case'/pages/500':
                            case'/login':
                            case'/register':
                                return $element.addClass('body-special');
                            case'/tasks':return $element.addClass('body-tasks');}
                    },addBg($location.path()),
                    $scope.$watch(path,function(newVal,oldVal){
                            return newVal!==oldVal?addBg($location.path()):void 0;}
                    );
            }]
        };
    }
);