'use strict';

angular.module('app', [])
    .run(['Global','$rootScope', '$location', function(Global, $rootScope, $location) {

    // register listener to watch route changes
    $rootScope.$on( '$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        console.log(Global);
        if ( !Global.authenticated ) {
            // no logged user, we should be going to #login
            if ( toState.name !== 'login' && toState.name !== 'register') {
                // already going to #login, no redirect needed
                $location.path( '/login' );
            }
        } else {
            if ( toState.name === 'login' || toState.name === 'register') {
                $location.path( '/app' );
            }
        }
    });
}]);
