'use strict';

//Setting up route
angular.module('mean.auth').config(['$stateProvider','$urlRouterProvider',
    function($stateProvider,$urlRouterProvider) {
        $urlRouterProvider.otherwise('/app');
        // states for my app
        $stateProvider
            .state('login', {
                url: '/login',
                views: {
                    'view' : {
                        templateUrl: 'public/auth/views/login.html'
                    }
                }
            })
            .state('register', {
                url: '/register',
                views: {
                    'view' : {
                        templateUrl: 'public/auth/views/register.html'
                    }
                }
            }).state('app',{
                url: '/app',
                views: {
                    'view' : {
                        templateUrl: 'public/app/views/dashboard.html'
                    }
                }
            });
    }
]);
