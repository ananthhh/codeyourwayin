'use strict';

//Setting up route
angular.module('mean.system')
    .config(['$locationProvider',
        function($locationProvider) {
            $locationProvider.hashPrefix('!');
        }
    ]);
